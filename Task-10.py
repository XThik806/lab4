#Дано рядки S, S1 і S2.  Замінити в рядку S останнє входження рядка S1 на рядок S2.

S = input("Введіть рядок: ")
s1 = "S1"
s2 = "S2"

def replace_last(s, s1, s2):
  last_index = s.rfind(s1)
  if last_index == -1:
    print("У рядку не знайдено 'S1', зміни не внесені")
  else: 
    print(s[:last_index] + s2 + s[last_index + len(s1):])

replace_last(S, s1, s2)