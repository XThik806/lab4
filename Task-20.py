#Дано список чисел.  Порахуйте, скільки в ньому пар елементів, рівних один одному. 
#Вважається, що будь-які два елементи, рівні один одному, утворюють одну пару, яку необхідно порахувати.

def count_pairs(list):
  pairs = 0
  for i in range(len(list)):
    ifPair = 1
    for j in range(i + 1, len(list)):
      if list[i] == list[j]:
        pairs += 1
        ifPair += 1
    print(f"if pairs = {ifPair}")
    if ifPair % 2 == 0:
      pairs += 1
  return pairs

n = int(input("Введіть кількість чисел: "))

list = [1]

for i in range(n):
  number = int(input("Введіть число: "))
  list.append(number)

pairs = count_pairs(list)

print(f"Кількість пар: {pairs}")
