#У списку чисел перевірити, чи всі елементи є унікальними, тобто кожне число зустрічається тільки один раз.

def is_unique(list):
  return len(list) == len(set(list))

n = int(input("Введіть кількість чисел: "))

list = []

for i in range(n):
  number = int(input("Введіть число: "))
  list.append(number)


if is_unique(list):
  print("Всі елементи списку унікальні")
else:
  print("У списку є дублікати")
